EESchema Schematic File Version 4
LIBS:full schematic 1-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_Small_US R5
U 1 1 60B7126A
P 3400 1900
F 0 "R5" H 3468 1946 50  0000 L CNN
F 1 "1220" H 3468 1855 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 3400 1900 50  0001 C CNN
F 3 "~" H 3400 1900 50  0001 C CNN
	1    3400 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R1
U 1 1 60B71270
P 1400 2550
F 0 "R1" H 1468 2596 50  0000 L CNN
F 1 "11.7k" H 1468 2505 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 1400 2550 50  0001 C CNN
F 3 "~" H 1400 2550 50  0001 C CNN
	1    1400 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D4
U 1 1 60B71276
P 1400 3400
F 0 "D4" V 1354 3479 50  0000 L CNN
F 1 "4.3V" V 1445 3479 50  0000 L CNN
F 2 "Diode_SMD:D_0402_1005Metric" H 1400 3400 50  0001 C CNN
F 3 "~" H 1400 3400 50  0001 C CNN
	1    1400 3400
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small_US R7
U 1 1 60B71282
P 2850 2950
F 0 "R7" V 2645 2950 50  0000 C CNN
F 1 "716" V 2736 2950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 2850 2950 50  0001 C CNN
F 3 "~" H 2850 2950 50  0001 C CNN
	1    2850 2950
	0    1    1    0   
$EndComp
$Comp
L Transistor_BJT:2N3904 Q1.2
U 1 1 60B71288
P 2650 1900
F 0 "Q1.2" V 2978 1900 50  0000 C CNN
F 1 "2N3904" V 2887 1900 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 2850 1825 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 2650 1900 50  0001 L CNN
	1    2650 1900
	0    -1   -1   0   
$EndComp
$Comp
L Transistor_BJT:2N3904 Q1.1
U 1 1 60B7128E
P 2850 1450
F 0 "Q1.1" V 3178 1450 50  0000 C CNN
F 1 "2N3904" V 3087 1450 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 3050 1375 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 2850 1450 50  0001 L CNN
	1    2850 1450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R_Small_US R4
U 1 1 60B712A1
P 3700 1350
F 0 "R4" V 3495 1350 50  0000 C CNN
F 1 "1.77" V 3586 1350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 3700 1350 50  0001 C CNN
F 3 "~" H 3700 1350 50  0001 C CNN
	1    3700 1350
	0    1    1    0   
$EndComp
Connection ~ 3400 1350
Wire Wire Line
	3400 1350 3600 1350
Wire Wire Line
	3400 1350 3400 1800
Wire Wire Line
	3400 2000 3400 2200
$Comp
L Device:R_Small_US R6
U 1 1 60B712B6
P 3400 2400
F 0 "R6" H 3468 2446 50  0000 L CNN
F 1 "10k" H 3468 2355 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 3400 2400 50  0001 C CNN
F 3 "~" H 3400 2400 50  0001 C CNN
	1    3400 2400
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:2N3904 Q2
U 1 1 60B712BC
P 4050 2850
F 0 "Q2" V 4378 2850 50  0000 C CNN
F 1 "2N3904" V 4287 2850 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 4250 2775 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 4050 2850 50  0001 L CNN
	1    4050 2850
	0    -1   1    0   
$EndComp
Connection ~ 3400 2200
Wire Wire Line
	3400 2200 3400 2300
Wire Wire Line
	3800 1350 4400 1350
Wire Wire Line
	4550 1350 4550 2550
$Comp
L Device:R_Small_US R2
U 1 1 60B712CE
P 4550 2650
F 0 "R2" H 4618 2696 50  0000 L CNN
F 1 "20k" H 4618 2605 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 4550 2650 50  0001 C CNN
F 3 "~" H 4550 2650 50  0001 C CNN
	1    4550 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R3
U 1 1 60B712D4
P 4550 3750
F 0 "R3" H 4618 3796 50  0000 L CNN
F 1 "10k" H 4618 3705 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 4550 3750 50  0001 C CNN
F 3 "~" H 4550 3750 50  0001 C CNN
	1    4550 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 3850 4550 4250
Wire Wire Line
	5150 1350 5150 2250
$Comp
L Device:R_Small_US R8
U 1 1 60B712F1
P 5150 2350
F 0 "R8" H 5218 2396 50  0000 L CNN
F 1 "1k" H 5218 2305 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" H 5150 2350 50  0001 C CNN
F 3 "~" H 5150 2350 50  0001 C CNN
	1    5150 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 1800 2850 1650
Wire Wire Line
	2450 1800 2450 1350
Wire Wire Line
	2450 1350 2650 1350
Wire Wire Line
	1400 2650 1400 2850
Wire Wire Line
	4550 2750 4550 3400
Wire Wire Line
	1750 2850 1400 2850
Connection ~ 1400 2850
Wire Wire Line
	1400 2850 1400 3250
Wire Wire Line
	2750 2950 2350 2950
Wire Wire Line
	2950 2950 3250 2950
Wire Wire Line
	3250 2950 3250 2250
Wire Wire Line
	3250 2250 2650 2250
Wire Wire Line
	2650 2250 2650 2100
Connection ~ 3250 2950
Wire Wire Line
	3050 1350 3400 1350
Wire Wire Line
	4050 2650 4050 2200
Wire Wire Line
	4050 2200 3400 2200
Wire Wire Line
	3250 2950 3850 2950
Wire Wire Line
	4400 2950 4400 1350
Wire Wire Line
	4400 2950 4250 2950
Connection ~ 4400 1350
Wire Wire Line
	4400 1350 4550 1350
Wire Wire Line
	1750 3050 1650 3050
Wire Wire Line
	1650 3400 4550 3400
Wire Wire Line
	1650 3050 1650 3400
Connection ~ 4550 3400
Wire Wire Line
	4550 3400 4550 3650
Wire Wire Line
	4550 1350 5150 1350
Connection ~ 4550 1350
Wire Wire Line
	1400 4250 3400 4250
Wire Wire Line
	1400 3550 1400 4250
Connection ~ 4550 4250
Wire Wire Line
	5150 4250 4550 4250
Wire Wire Line
	5150 2450 5150 4250
$Comp
L Amplifier_Operational:LM358 U1
U 1 1 60BDB940
P 2050 2950
F 0 "U1" H 2050 3317 50  0000 C CNN
F 1 "LM358" H 2050 3226 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm_LongPads" H 2050 2950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm2904-n.pdf" H 2050 2950 50  0001 C CNN
	1    2050 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 2450 1400 1350
Wire Wire Line
	1400 1350 2450 1350
Connection ~ 2450 1350
Wire Wire Line
	1400 1350 1400 1150
Connection ~ 1400 1350
Text GLabel 1400 1150 0    50   Input ~ 0
control
Wire Wire Line
	3400 2500 3400 4250
Connection ~ 3400 4250
Wire Wire Line
	3400 4250 3750 4250
Wire Wire Line
	3750 4250 3750 4600
Connection ~ 3750 4250
Wire Wire Line
	3750 4250 4550 4250
$Comp
L power:GND #PWR?
U 1 1 60BBEC3C
P 3750 4600
F 0 "#PWR?" H 3750 4350 50  0001 C CNN
F 1 "GND" H 3755 4427 50  0000 C CNN
F 2 "" H 3750 4600 50  0001 C CNN
F 3 "" H 3750 4600 50  0001 C CNN
	1    3750 4600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1350 6150 1350
Wire Wire Line
	6150 1350 6150 1400
Connection ~ 5150 1350
Text GLabel 6150 1400 2    50   Input ~ 0
Output
$EndSCHEMATC
